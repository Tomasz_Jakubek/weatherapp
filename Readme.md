# A Guide to Initializing the WeatherApp

This React app is meant to display the temperature, humidity and cloud coverage for major cities in Poland using Redux and Redux Thunk. The app is updated every 10 minutes.

## Prerequisites

[Node.js](http://nodejs.org/) must be installed.

## Installation

- First, clone the repository from bitbucket
- Running `yarn` or `npm i` in the app's root directory will install everything you need for development.

## Development Server

- `yarn dev` will run the app's development server at [http://localhost:8080](http://localhost:3000) 
- `yarn watch` will enable hot reloading
- `yarn start` will run both dev and watch