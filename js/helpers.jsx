export function sortCities(props, value, direction) {
  const target = value;
  return props.cities.sort(function(a, b) {
    if (direction === 'descending') {
      if (a[target] > b[target]) return -1;
      else if (a[target] < b[target]) return 1;
      if (a[target] === b[target]) {
        const x = a.name;
        const y = b.name;
        return x > y ? 1 : x < y ? -1 : 0;
      }
    } else {
      if (a[target] < b[target]) return -1;
      else if (a[target] > b[target]) return 1;
      if (a[target] === b[target]) {
        const x = a.name;
        const y = b.name;
        return x > y ? 1 : x < y ? -1 : 0;
      }
    }
  });
}
