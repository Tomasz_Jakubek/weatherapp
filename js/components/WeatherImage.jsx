import React from 'react';
import PropTypes from 'prop-types';

const WeatherImage = props => (
  <div>
    <img src={`../../public/img/${props.icon}.png`} alt="Description of current weather" />
  </div>
);

WeatherImage.propTypes = {
  icon: PropTypes.string
};

export default WeatherImage;
