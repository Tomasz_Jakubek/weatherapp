import React from 'react';
import PropTypes from 'prop-types';
import Card from '@material-ui/core/Card';
import Temperature from './Temperature';
import WeatherImage from './WeatherImage';
import Cloud from './Cloud';
import Wind from './Wind';
import Humidity from './Humidity';
import Pressure from './Pressure';

const className = `props.card city`;

const City = props => (
  <Card className={className}>

    <div className={'city__info_main-details'}>
      <h2 className={'city__headline'}>
        {props.name}
      </h2>
      <div className="city__info__imp-details">
        <Temperature temperature={props.temperature} />
        <WeatherImage icon={props.icon} />
      </div>
      <Cloud cloudCover={props.cloudCover} weatherDesc={props.weatherDesc} icon={props.icon} />
    </div>
    <div className="city__info__min-details">
      <Humidity humidity={props.humidity} />
      <Pressure pressure={props.pressure} />
      <Wind windSpeed={props.windSpeed} windDirection={props.windDirection} />
    </div>

  </Card>
);

City.propTypes = {
  name: PropTypes.string,
  temperature: PropTypes.number,
  cloudCover: PropTypes.number,
  weatherDesc: PropTypes.string,
  icon: PropTypes.string,
  humidity: PropTypes.number,
  pressure: PropTypes.string,
  windSpeed: PropTypes.number,
  windDirection: PropTypes.number
};

export default City;
