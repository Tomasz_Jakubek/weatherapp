import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

class Wind extends React.Component {
  static propTypes = {
    windDirection: PropTypes.number,
    windSpeed: PropTypes.number
  };

  render() {
    const Wrapper = styled.img`
            transform: rotate(${this.props.windDirection}deg);
            width: 0.9rem;
            height: 0.9rem;
            `;
    return (
      <div className={'city__info__min-spec-details'}>
        <h4>Wind</h4>
        <div className={'wind--box'}>
          <p>{(this.props.windSpeed / 1000 * 3600).toFixed(0)}km/h</p>
          <Wrapper src="../../public/img/windArrow.png" alt="" />
        </div>
      </div>
    );
  }
}

export default Wind;
