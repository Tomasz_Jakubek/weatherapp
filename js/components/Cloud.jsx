import React from 'react';
import PropTypes from 'prop-types';

const Cloud = props => (
  <div>
    <p>Cloud coverage: {props.cloudCover}%</p>
    <h3>{props.weatherDesc}</h3>
  </div>
);

Cloud.propTypes = {
  cloudCover: PropTypes.number,
  weatherDesc: PropTypes.string
};

export default Cloud;
