import React from 'react';
import { connect } from 'react-redux';
import Header from './Header';
import DataFetch from './DataFetch';
import { getWindowSize } from '../store/actionCreators';

export class Homepage extends React.Component {
  constructor(props) {
    super(props);
    this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
  }

  componentDidMount() {
    this.updateWindowDimensions();
    window.addEventListener('resize', this.updateWindowDimensions);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateWindowDimensions);
  }

  updateWindowDimensions() {
    this.props.actions.getWindowSize({ width: window.innerWidth, height: window.innerHeight });
  }

  render() {
    return (
      <div className={'homepage'}>
        <Header />
        <DataFetch />
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    actions: {
      getWindowSize: windowSize => dispatch(getWindowSize(windowSize))
    }
  };
};

export default connect(undefined, mapDispatchToProps)(Homepage);
