import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getWeatherData } from '../store/actionCreators';
import Error from './Error';
import Loading from './Loading';
import Results from './Results';

export class DataFetch extends React.Component {
  static propTypes = {
    cities: PropTypes.arrayOf(
      PropTypes.shape({
        name: PropTypes.string,
        temperature: PropTypes.number,
        cloudCover: PropTypes.number,
        weatherDesc: PropTypes.string,
        icon: PropTypes.string,
        humidity: PropTypes.number,
        pressure: PropTypes.string,
        windSpeed: PropTypes.number,
        windDirection: PropTypes.number
      })
    ),
    hasErrored: PropTypes.bool,
    isLoading: PropTypes.bool,
    actions: PropTypes.shape({
      fetchData: PropTypes.func
    })
  };

  constructor(props) {
    super(props);
    this.code = <div />;
    this.cityIDs = [
      776069,
      3102014,
      3099434,
      3098722,
      3096472,
      769250,
      3094802,
      765876,
      3093133,
      763166,
      3090048,
      3088171,
      759734,
      3083829,
      3083271,
      756135,
      3081368,
      3080165
    ].join(',');
    this.url = `http://api.openweathermap.org/data/2.5/group?id=${this.cityIDs}&APPID=8060242c069ec488f69f99a6c99e92e8`;
  }

  componentDidMount() {
    this.props.actions.fetchData(this.url);
    this.interval = setInterval(
      function updateData2() {
        this.props.actions.fetchData(this.url);
      }.bind(this),
      600000
    );
  }

  shouldComponentUpdate(nextprops) {
    return this.props !== nextprops;
  }

  componentWillUpdate(props) {
    if (props.hasErrored === true) {
      this.code = <Error />;
      clearInterval(this.interval);
    } else if (props.isLoading === true) {
      this.code = <Loading />;
    } else if (props.cities.length > 0) {
      this.code = <Results />;
    }
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    return this.code;
  }
}

const mapStateToProps = state => {
  return {
    cities: state.cities,
    hasErrored: state.hasErrored,
    isLoading: state.isLoading
  };
};

const mapDispatchToProps = dispatch => {
  return {
    actions: {
      fetchData: url => dispatch(getWeatherData(url))
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DataFetch);
