import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { setSort, setTemperatureSymbol } from '../store/actionCreators';
import { sortCities } from '../helpers';

export class ButtonMenu extends React.Component {
  constructor(props) {
    super(props);
    this.code = <div />;
    this.state = {
      anchorEl: null
    };
    this.className = `props.button menuButton`;
  }

  handleClick = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  static propTypes = {
    cities: PropTypes.arrayOf(
      PropTypes.shape({
        name: PropTypes.string,
        temperature: PropTypes.number,
        cloudCover: PropTypes.number,
        weatherDesc: PropTypes.string,
        icon: PropTypes.string,
        humidity: PropTypes.number,
        pressure: PropTypes.string,
        windSpeed: PropTypes.number,
        windDirection: PropTypes.number
      })
    ),
    sort: PropTypes.shape({
      direction: PropTypes.string,
      term: PropTypes.string
    }),
    temperatureSymbol: PropTypes.string,
    actions: PropTypes.shape({
      setSort: PropTypes.func,
      setTemperatureSymbol: PropTypes.func
    })
  };

  dispatchSort = value => {
    const direction = this.props.sort.direction === 'ascending' && this.props.sort.term === value
      ? 'descending'
      : 'ascending';
    const sortObject = {
      term: value,
      direction
    };
    const cityTemp = sortCities(this.props, value, direction);
    const stateObject = {
      cities: [...cityTemp],
      sort: sortObject
    };
    this.props.actions.setSort(stateObject);
  };

  dispatchTemperatureSymbol = () => {
    this.props.temperatureSymbol === 'celsius'
      ? this.props.actions.setTemperatureSymbol('fahrenheit')
      : this.props.actions.setTemperatureSymbol('celsius');
  };

  render() {
    const { anchorEl } = this.state;
    return (
      <div>
        <Button
          variant="contained"
          color="primary"
          aria-owns={anchorEl ? 'simple-menu' : null}
          aria-haspopup="true"
          onClick={this.handleClick}
        >
          Menu
        </Button>
        <Menu id="simple-menu" anchorEl={anchorEl} open={Boolean(anchorEl)} onClose={this.handleClose}>
          <MenuItem>
            <Button
              size="small"
              variant="contained"
              color="primary"
              className={this.className}
              onClick={() => this.dispatchSort('name')}
            >
              Name
            </Button>
          </MenuItem>
          <MenuItem>
            <Button
              size="small"
              variant="contained"
              color="primary"
              className={this.className}
              onClick={() => this.dispatchSort('temperature')}
            >
              Temperature
            </Button>
          </MenuItem>
          <MenuItem>
            <Button
              size="small"
              variant="contained"
              color="primary"
              className={this.className}
              onClick={() => this.dispatchSort('humidity')}
            >
              Humidity
            </Button>
          </MenuItem>
          <MenuItem>
            <Button
              size="small"
              variant="contained"
              color="primary"
              className={this.className}
              onClick={() => this.dispatchSort('pressure')}
            >
              Pressure
            </Button>
          </MenuItem>
          <MenuItem>
            <Button
              size="small"
              variant="contained"
              color="primary"
              className={this.className}
              onClick={() => this.dispatchSort('cloudCover')}
            >
              Cloud
            </Button>
          </MenuItem>
          <MenuItem>
            <Button
              size="small"
              variant="contained"
              color="primary"
              className={this.className}
              onClick={() => this.dispatchSort('windSpeed')}
            >
              Wind
            </Button>
          </MenuItem>
          <MenuItem>
            <Button
              size="small"
              variant="contained"
              color="secondary"
              className={this.className}
              onClick={() => this.dispatchTemperatureSymbol('celsius')}
            >
              °C / °F
            </Button>
          </MenuItem>
        </Menu>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    cities: state.cities,
    sort: state.sortObject,
    temperatureSymbol: state.temperatureSymbol
  };
};

const mapDispatchToProps = dispatch => {
  return {
    actions: {
      setSort: data => dispatch(setSort(data)),
      setTemperatureSymbol: value => dispatch(setTemperatureSymbol(value))
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ButtonMenu);
