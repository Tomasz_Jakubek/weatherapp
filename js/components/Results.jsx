import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import City from './City';

export class Results extends React.Component {
  static propTypes = {
    cities: PropTypes.arrayOf(
      PropTypes.shape({
        name: PropTypes.string,
        temperature: PropTypes.number,
        cloudCover: PropTypes.number,
        weatherDesc: PropTypes.string,
        icon: PropTypes.string,
        humidity: PropTypes.number,
        pressure: PropTypes.string,
        windSpeed: PropTypes.number,
        windDirection: PropTypes.number
      })
    )
  };

  render() {
    return (
      <div className={'home'}>
        <div className={'city-list'}>
          {this.props.cities.map(el => <City {...el} key={el.name} />)}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    cities: state.cities
  };
};

export default connect(mapStateToProps)(Results);
