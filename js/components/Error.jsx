import React from 'react';

const Error = () => (
  <div className={'error'}>
    <h1>OOPS!</h1>
    <h3>There seems to be an error, please reload the page or check your internet connection</h3>
  </div>
);

export default Error;
