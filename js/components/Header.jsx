import React from 'react';
import Nav from './Nav';

const Header = () => (
  <header>
    <h1>
      Current weather for Polish cities
    </h1>
    <Nav />
  </header>
);

export default Header;
