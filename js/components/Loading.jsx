import React from 'react';
import Spinner from './Spinner';

const Error = () => (
  <div className={'loading'}>
    <h1>LOADING!</h1>
    <h3>Thank you for waiting patiently</h3>
    <Spinner />
  </div>
);

export default Error;
