import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

export class Temperature extends React.Component {
  static propTypes = {
    temperatureSymbol: PropTypes.string,
    temperature: PropTypes.number
  };

  componentWillMount() {
    if (this.props.temperatureSymbol === 'celsius') {
      this.code = <h3>{(this.props.temperature - 273).toFixed(0)} <sup>o</sup>C</h3>;
    } else if (this.props.temperatureSymbol === 'fahrenheit') {
      this.code = <h3>{(9 / 5 * (this.props.temperature - 273) + 32).toFixed(0)} <sup>o</sup>F</h3>;
    }
  }

  shouldComponentUpdate(nextprops) {
    return this.props.temperatureSymbol !== nextprops.temperatureSymbol;
  }

  componentWillUpdate(props) {
    if (props.temperatureSymbol === 'celsius') {
      this.code = <h3>{(props.temperature - 273).toFixed(0)} <sup>o</sup>C</h3>;
    } else if (props.temperatureSymbol === 'fahrenheit') {
      this.code = <h3>{(9 / 5 * (props.temperature - 273) + 32).toFixed(0)} <sup>o</sup>F</h3>;
    }
  }

  render() {
    return this.code;
  }
}
const mapStateToProps = state => {
  return {
    temperatureSymbol: state.temperatureSymbol
  };
};

export default connect(mapStateToProps)(Temperature);
