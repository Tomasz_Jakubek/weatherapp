import React from 'react';
import PropTypes from 'prop-types';

const Pressure = props => (
  <div className={'city__info__min-spec-details'}>
    <h4>Pressure </h4>
    <p>{props.pressure} hPa</p>
  </div>
);

Pressure.propTypes = {
  pressure: PropTypes.string
};

export default Pressure;
