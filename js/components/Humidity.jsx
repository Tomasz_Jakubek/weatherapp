import React from 'react';
import PropTypes from 'prop-types';

const Humidity = props => (
  <div className={'city__info__min-spec-details'}>
    <h4>Humidity</h4>
    <p>{props.humidity}%</p>
  </div>
);

Humidity.propTypes = {
  humidity: PropTypes.number
};

export default Humidity;
