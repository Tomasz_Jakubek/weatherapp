import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ButtonList from './ButtonList';
import ButtonMenu from './ButtonMenu';

export class Nav extends React.Component {
  constructor(props) {
    super(props);
    this.code = <div />;
  }

  static propTypes = {
    windowSize: PropTypes.shape({
      width: PropTypes.number,
      height: PropTypes.number
    })
  };

  shouldComponentUpdate(nextprops) {
    return this.props !== nextprops;
  }

  componentWillUpdate(props) {
    if (props.windowSize.width > 1000) {
      this.code = <ButtonList />;
    } else if (props.windowSize.width !== 0) {
      this.code = <ButtonMenu />;
    }
  }

  render() {
    return this.code;
  }
}

const mapStateToProps = state => {
  return {
    windowSize: state.windowSize
  };
};

export default connect(mapStateToProps)(Nav);
