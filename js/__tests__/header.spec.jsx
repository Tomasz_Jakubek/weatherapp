import React from 'react';
import { shallow } from 'enzyme/build/index';
import Nav from '../components/Nav';
import Header from '../components/Header';

describe('Header', () => {
  let mountedHeader;
  const header = () => {
    if (!mountedHeader) {
      mountedHeader = shallow(<Header />);
    }
    return mountedHeader;
  };

  beforeEach(() => {
    mountedHeader = undefined;
  });

  it('Header should be rendered', () => {
    const component = shallow(<Header />);

    expect(component).toMatchSnapshot();
  });

  it('always renders a header', () => {
    const headertag = header().find('header');
    expect(headertag.length).toBeGreaterThan(0);
  });
  it('always renders a h1', () => {
    const h1 = header().find('h1');
    expect(h1.length).toBeGreaterThan(0);
  });

  it('always renders a Nav', () => {
    expect(header().find(Nav).length).toBe(1);
  });

  describe('rendered `Nav`', () => {
    it('does not receive any props', () => {
      const nav = header().find(Nav);
      expect(Object.keys(nav.props()).length).toBe(0);
    });
  });

  describe('the rendered header', () => {
    it('contains everything else that gets rendered', () => {
      const divs = header().find('header');
      const wrappingDiv = divs.first();

      expect(wrappingDiv.children()).toEqual(header().children());
    });
  });
});
