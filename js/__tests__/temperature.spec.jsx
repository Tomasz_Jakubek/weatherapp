import React from 'react';
import { shallow } from 'enzyme/build/index';
import { Temperature } from '../components/Temperature';

describe('Temperature', () => {
  let mountedTemperature;
  let props;
  const temperature = () => {
    if (!mountedTemperature) {
      mountedTemperature = shallow(<Temperature temperatureSymbol={props.temperatureSymbol} />);
    }
    return mountedTemperature;
  };

  beforeEach(() => {
    props = {
      temperatureSymbol: 'celsius'
    };
    mountedTemperature = undefined;
  });

  it('Temperature should be rendered', () => {
    expect(temperature).toMatchSnapshot();
  });

  it('always renders a single h3', () => {
    const p = temperature().find('h3');
    expect(p.length).toBeGreaterThan(0);
    expect(p.length).toEqual(1);
  });
});
