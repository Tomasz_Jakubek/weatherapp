import React from 'react';
import { shallow } from 'enzyme/build/index';
import { Nav } from '../components/Nav';
import ButtonList from '../components/ButtonList';

describe('Nav', () => {
  let mountedNav;
  const nav = () => {
    if (!mountedNav) {
      mountedNav = shallow(<Nav />);
    }
    return mountedNav;
  };

  beforeEach(() => {
    mountedNav = undefined;
  });

  it('Nav should be rendered', () => {
    expect(nav).toMatchSnapshot();
  });
});
