import * as actionsFromCreators from '../store/actionCreators';
import * as actions from '../store/actions';

describe('setTemperatureSymbol', () => {
  it(`should create an action to set the chosen temperature symbol`, () => {
    const temperatureSymbol = 'celsius';
    const expectedAction = {
      payload: temperatureSymbol,
      type: actions.SET_TEMPERATURE_SYMBOL
    };
    expect(actionsFromCreators.setTemperatureSymbol(temperatureSymbol)).toEqual(expectedAction);
  });
});

describe('weatherDataHasErrored', () => {
  it(`should create an action to set 'hasErrored' to 'true `, () => {
    const error = true;
    const expectedAction = {
      payload: error,
      type: actions.WEATHER_DATA_HAS_ERRORED
    };
    expect(actionsFromCreators.weatherDataHasErrored(error)).toEqual(expectedAction);
  });
});

describe('weatherDataIsLoading', () => {
  it(`should create an action to set 'isLoading' to 'true' `, () => {
    const loading = true;
    const expectedAction = {
      payload: loading,
      type: actions.WEATHER_DATA_IS_LOADING
    };
    expect(actionsFromCreators.weatherDataIsLoading(loading)).toEqual(expectedAction);
  });

  it(`should create an action to set 'isLoading' to 'false' `, () => {
    const loading = false;
    const expectedAction = {
      payload: loading,
      type: actions.WEATHER_DATA_IS_LOADING
    };
    expect(actionsFromCreators.weatherDataIsLoading(loading)).toEqual(expectedAction);
  });
});

describe('weatherDataFetchSuccess', () => {
  it(`should create an action to set 'weatherData' to an array of objects `, () => {
    const data = [{ name: 'city1' }, { name: 'city2' }];
    const expectedAction = {
      payload: data,
      type: actions.WEATHER_DATA_FETCH_SUCCESS
    };
    expect(actionsFromCreators.weatherDataFetchSuccess(data)).toEqual(expectedAction);
  });
});

describe('getWindowSize', () => {
  it(`should create an action to set 'windowSize'  `, () => {
    const windowSize = {
      windowSize: {
        width: 100,
        height: 100
      }
    };
    const expectedAction = {
      payload: windowSize,
      type: actions.GET_WINDOW_SIZE
    };
    expect(actionsFromCreators.getWindowSize(windowSize)).toEqual(expectedAction);
  });
});

describe('setSort', () => {
  it(`should create an action to set 'windowSize'  `, () => {
    const sortObject = {
      term: 'name',
      direction: 'ascending'
    };
    const cities = [{ name: 'city1' }, { name: 'city2' }];
    const payload = {
      sortObject,
      cities
    };

    const expectedAction = {
      payload: payload,
      type: actions.SET_SORT
    };
    expect(actionsFromCreators.setSort(payload)).toEqual(expectedAction);
  });
});
