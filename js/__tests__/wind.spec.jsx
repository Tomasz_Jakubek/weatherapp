import React from 'react';
import { shallow } from 'enzyme/build/index';
import Wind from '../components/Wind';

describe('Wind', () => {
  let mountedWind;
  const wind = () => {
    if (!mountedWind) {
      mountedWind = shallow(<Wind />);
    }
    return mountedWind;
  };

  beforeEach(() => {
    mountedWind = undefined;
  });

  it('Wind should be rendered', () => {
    expect(wind).toMatchSnapshot();
  });

  it('always renders a div', () => {
    const divs = wind().find('div');
    expect(divs.length).toBeGreaterThan(0);
  });

  describe('the rendered div', () => {
    it('contains everything else that gets rendered', () => {
      const divs = wind().find('div');
      const wrappingDiv = divs.first();
      expect(wrappingDiv.children()).toEqual(wind().children());
    });
  });
});
