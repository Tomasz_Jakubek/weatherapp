import React from 'react';
import { shallow } from 'enzyme/build/index';

import Cloud from '../components/Cloud';

describe('Cloud', () => {
  let mountedCloud;
  const cloud = () => {
    if (!mountedCloud) {
      mountedCloud = shallow(<Cloud />);
    }
    return mountedCloud;
  };

  beforeEach(() => {
    mountedCloud = undefined;
  });

  it('Cloud should be rendered', () => {
    expect(cloud).toMatchSnapshot();
  });

  it('always renders a div', () => {
    const divs = cloud().find('div');
    expect(divs.length).toBeGreaterThan(0);
  });

  it('always renders a p', () => {
    const p = cloud().find('p');
    expect(p.length).toBeGreaterThan(0);
    expect(p.length).toEqual(1);
  });
  it('always renders a h3', () => {
    const p = cloud().find('h3');
    expect(p.length).toBeGreaterThan(0);
    expect(p.length).toEqual(1);
  });

  describe('the rendered div', () => {
    it('contains everything else that gets rendered', () => {
      const divs = cloud().find('div');
      const wrappingDiv = divs.first();

      expect(wrappingDiv.children()).toEqual(cloud().children());
    });
  });
});
