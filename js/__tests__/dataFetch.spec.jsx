import React from 'react';
import { shallow } from 'enzyme/build/index';

import DataFetch from '../components/DataFetch';

describe('DataFetch', () => {
  let mountedDataFetch;
  const datafetch = () => {
    if (!mountedDataFetch) {
      mountedDataFetch = shallow(<DataFetch />);
    }
    return mountedDataFetch;
  };

  beforeEach(() => {
    mountedDataFetch = undefined;
  });

  it('DataFetch should be rendered', () => {
    expect(datafetch).toMatchSnapshot();
  });
});
