import React from 'react';
import { shallow } from 'enzyme/build/index';
import Loading from '../components/Loading';
import Spinner from '../components/Spinner';

describe('Loading', () => {
  let mountedLoading;
  const loading = () => {
    if (!mountedLoading) {
      mountedLoading = shallow(<Loading />);
    }
    return mountedLoading;
  };

  beforeEach(() => {
    mountedLoading = undefined;
  });

  it('Loading should be rendered', () => {
    expect(loading).toMatchSnapshot();
  });

  it('always renders a div', () => {
    const divs = loading().find('div');
    expect(divs.length).toBeGreaterThan(0);
  });
  it('always renders a h1', () => {
    const h1 = loading().find('h1');
    expect(h1.length).toBeGreaterThan(0);
  });
  it('always renders a h3', () => {
    const h3 = loading().find('h3');
    expect(h3.length).toBeGreaterThan(0);
  });
  it('always renders a Spinner', () => {
    const spinner = loading().find(Spinner);
    expect(spinner.length).toBeGreaterThan(0);
  });

  describe('rendered `Spinner`', () => {
    it('does not receive any props', () => {
      const spinner = loading().find(Spinner);
      expect(Object.keys(spinner.props()).length).toBe(0);
    });
  });

  describe('the rendered div', () => {
    it('contains everything else that gets rendered', () => {
      const divs = loading().find('div');
      const wrappingDiv = divs.first();

      expect(wrappingDiv.children()).toEqual(loading().children());
    });
  });
});
