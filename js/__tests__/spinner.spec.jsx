import React from 'react';
import { shallow } from 'enzyme/build/index';
import Spinner from '../components/Spinner';

describe('Spinner', () => {
  let mountedSpinner;
  const spinner = () => {
    if (!mountedSpinner) {
      mountedSpinner = shallow(<Spinner />);
    }
    return mountedSpinner;
  };

  beforeEach(() => {
    mountedSpinner = undefined;
  });

  it('Spinner should be rendered', () => {
    expect(spinner).toMatchSnapshot();
  });
});
