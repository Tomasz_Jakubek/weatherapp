import React from 'react';
import { shallow } from 'enzyme/build/index';
import City from '../components/City';
import { Results } from '../components/Results';

describe('Results', () => {
  let mountedResults;
  const results = () => {
    if (!mountedResults) {
      mountedResults = shallow(<Results cities={[]} />);
    }
    return mountedResults;
  };

  beforeEach(() => {
    mountedResults = undefined;
  });

  it('Results should be rendered', () => {
    expect(results).toMatchSnapshot();
  });

  it('always renders a div', () => {
    const divs = results().find('div');
    expect(divs.length).toBeGreaterThan(0);
  });

  describe('the rendered div', () => {
    it('contains everything else that gets rendered', () => {
      const divs = results().find('div');
      const wrappingDiv = divs.first();

      expect(wrappingDiv.children()).toEqual(results().children());
    });
  });

  test('2 City tags should be rendered', () => {
    const component = shallow(<Results cities={[{ name: 'city1' }, { name: 'city2' }]} />);
    const cities = component.find(City);
    expect(cities.length).toEqual(2);
  });
  test('4 City tags should be rendered', () => {
    const component = shallow(
      <Results cities={[{ name: 'city1' }, { name: 'city2' }, { name: 'city3' }, { name: 'city4' }]} />
    );
    const cities = component.find(City);
    expect(cities.length).toEqual(4);
  });
});
