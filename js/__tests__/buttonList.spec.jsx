import React from 'react';
import { shallow } from 'enzyme/build/index';
import Button from '@material-ui/core/Button';
import { ButtonList } from '../components/ButtonList';

describe('ButtonList', () => {
  let mountedButtonList;
  const buttonlist = () => {
    if (!mountedButtonList) {
      mountedButtonList = shallow(<ButtonList />);
    }
    return mountedButtonList;
  };

  beforeEach(() => {
    mountedButtonList = undefined;
  });

  it('ButtonList should be rendered', () => {
    expect(buttonlist).toMatchSnapshot();
  });

  it('always renders a div', () => {
    const divs = buttonlist().find('div');
    expect(divs.length).toBeGreaterThan(0);
  });

  describe('the rendered div', () => {
    it('contains everything else that gets rendered', () => {
      const divs = buttonlist().find('div');
      const wrappingDiv = divs.first();

      expect(wrappingDiv.children()).toEqual(buttonlist().children());
    });
  });

  test('7 Button tags should be rendered', () => {
    const component = shallow(<ButtonList />);
    const cities = component.find(Button);
    expect(cities.length).toEqual(7);
  });
});
