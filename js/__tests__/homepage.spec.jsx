import React from 'react';
import { shallow } from 'enzyme/build/index';
import { Homepage } from '../components/Homepage';
import Header from '../components/Header';
import DataFetch from '../components/DataFetch';

describe('Homepage', () => {
  let mountedHomepage;
  const homepage = () => {
    if (!mountedHomepage) {
      mountedHomepage = shallow(<Homepage />);
    }
    return mountedHomepage;
  };

  beforeEach(() => {
    mountedHomepage = undefined;
  });

  it('Homepage should be rendered', () => {
    const component = shallow(<Homepage />);

    expect(component).toMatchSnapshot();
  });

  it('always renders a div', () => {
    const divs = homepage().find('div');
    expect(divs.length).toBeGreaterThan(0);
  });

  describe('the rendered div', () => {
    it('contains everything else that gets rendered', () => {
      const divs = homepage().find('div');
      const wrappingDiv = divs.first();

      expect(wrappingDiv.children()).toEqual(homepage().children());
    });
  });

  it('always renders a `DataFetch`', () => {
    expect(homepage().find(DataFetch).length).toBe(1);
  });

  it('always renders a `Header`', () => {
    expect(homepage().find(Header).length).toBe(1);
  });

  describe('rendered `DataFetch`', () => {
    it('does not receive any props', () => {
      const datafetch = homepage().find(DataFetch);
      expect(Object.keys(datafetch.props()).length).toBe(0);
    });
  });

  describe('rendered `Header`', () => {
    it('does not receive any props', () => {
      const header = homepage().find(Header);
      expect(Object.keys(header.props()).length).toBe(0);
    });
  });
});
