import React from 'react';
import { shallow } from 'enzyme/build/index';
import Pressure from '../components/Pressure';

describe('Pressure', () => {
  let mountedPressure;
  const pressure = () => {
    if (!mountedPressure) {
      mountedPressure = shallow(<Pressure />);
    }
    return mountedPressure;
  };

  beforeEach(() => {
    mountedPressure = undefined;
  });

  it('Pressure should be rendered', () => {
    expect(pressure).toMatchSnapshot();
  });

  it('always renders a div', () => {
    const divs = pressure().find('div');
    expect(divs.length).toBeGreaterThan(0);
  });
  it('always renders a h4', () => {
    const h4 = pressure().find('h4');
    expect(h4.length).toBeGreaterThan(0);
  });
  it('always renders a p', () => {
    const p = pressure().find('p');
    expect(p.length).toBeGreaterThan(0);
  });

  describe('the rendered div', () => {
    it('contains everything else that gets rendered', () => {
      const divs = pressure().find('div');
      const wrappingDiv = divs.first();
      expect(wrappingDiv.children()).toEqual(pressure().children());
    });
  });
});
