import React from 'react';
import { shallow } from 'enzyme/build/index';
import Button from '@material-ui/core/Button';
import { ButtonMenu } from '../components/ButtonMenu';

describe('ButtonMenu', () => {
  let mountedButtonMenu;
  const buttonmenu = () => {
    if (!mountedButtonMenu) {
      mountedButtonMenu = shallow(<ButtonMenu />);
    }
    return mountedButtonMenu;
  };

  beforeEach(() => {
    mountedButtonMenu = undefined;
  });

  it('ButtonMenu should be rendered', () => {
    expect(buttonmenu).toMatchSnapshot();
  });

  it('always renders a div', () => {
    const divs = buttonmenu().find('div');
    expect(divs.length).toBeGreaterThan(0);
  });

  describe('the rendered div', () => {
    it('contains everything else that gets rendered', () => {
      const divs = buttonmenu().find('div');
      const wrappingDiv = divs.first();

      expect(wrappingDiv.children()).toEqual(buttonmenu().children());
    });
  });

  test('8 Button tags should be rendered, 7 from menu', () => {
    const component = shallow(<ButtonMenu />);
    const cities = component.find(Button);
    expect(cities.length).toEqual(8);
  });
});
