import React from 'react';
import { shallow } from 'enzyme/build/index';

import WeatherImage from '../components/WeatherImage';

describe('WeatherImage', () => {
  let mountedWeatherImage;
  const weatherimage = () => {
    if (!mountedWeatherImage) {
      mountedWeatherImage = shallow(<WeatherImage />);
    }
    return mountedWeatherImage;
  };

  beforeEach(() => {
    mountedWeatherImage = undefined;
  });

  it('WeatherImage should be rendered', () => {
    expect(weatherimage).toMatchSnapshot();
  });

  it('always renders a div', () => {
    const divs = weatherimage().find('div');
    expect(divs.length).toBeGreaterThan(0);
  });

  it('always renders a img', () => {
    const img = weatherimage().find('img');
    expect(img.length).toBeGreaterThan(0);
    expect(img.length).toEqual(1);
  });

  describe('the rendered div', () => {
    it('contains everything else that gets rendered', () => {
      const divs = weatherimage().find('div');
      const wrappingDiv = divs.first();

      expect(wrappingDiv.children()).toEqual(weatherimage().children());
    });
  });
});
