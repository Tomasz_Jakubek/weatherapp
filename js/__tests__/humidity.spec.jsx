import React from 'react';
import { shallow } from 'enzyme/build/index';
import Humidity from '../components/Humidity';

describe('Humidity', () => {
  let mountedHumidity;
  const humidity = () => {
    if (!mountedHumidity) {
      mountedHumidity = shallow(<Humidity />);
    }
    return mountedHumidity;
  };

  beforeEach(() => {
    mountedHumidity = undefined;
  });

  it('Humidity should be rendered', () => {
    expect(humidity).toMatchSnapshot();
  });

  it('always renders a div', () => {
    const divs = humidity().find('div');
    expect(divs.length).toBeGreaterThan(0);
  });
  it('always renders a h4', () => {
    const h4 = humidity().find('h4');
    expect(h4.length).toBeGreaterThan(0);
  });
  it('always renders a p', () => {
    const p = humidity().find('p');
    expect(p.length).toBeGreaterThan(0);
  });

  describe('the rendered div', () => {
    it('contains everything else that gets rendered', () => {
      const divs = humidity().find('div');
      const wrappingDiv = divs.first();

      expect(wrappingDiv.children()).toEqual(humidity().children());
    });
  });
});
