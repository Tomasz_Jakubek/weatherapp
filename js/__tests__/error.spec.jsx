import React from 'react';
import { shallow } from 'enzyme/build/index';

import Error from '../components/Error';

describe('Error', () => {
  let mountedError;
  const error = () => {
    if (!mountedError) {
      mountedError = shallow(<Error />);
    }
    return mountedError;
  };

  beforeEach(() => {
    mountedError = undefined;
  });

  it('Error should be rendered', () => {
    expect(error).toMatchSnapshot();
  });

  it('always renders a div', () => {
    const divs = error().find('div');
    expect(divs.length).toBeGreaterThan(0);
  });
  it('always renders a h1', () => {
    const h1 = error().find('h1');
    expect(h1.length).toBeGreaterThan(0);
  });
  it('always renders a h3', () => {
    const h3 = error().find('h3');
    expect(h3.length).toBeGreaterThan(0);
  });

  describe('the rendered div', () => {
    it('contains everything else that gets rendered', () => {
      const divs = error().find('div');
      const wrappingDiv = divs.first();

      expect(wrappingDiv.children()).toEqual(error().children());
    });
  });
});
