import {
  GET_WEATHER_DATA,
  WEATHER_DATA_HAS_ERRORED,
  WEATHER_DATA_IS_LOADING,
  WEATHER_DATA_FETCH_SUCCESS,
  SET_SORT,
  SET_TEMPERATURE_SYMBOL,
  GET_WINDOW_SIZE
} from './actions';

const DEFAULT_STATE = {
  cities: [],
  hasErrored: false,
  isLoading: false,
  sortObject: {
    term: 'name',
    direction: 'ascending'
  },
  temperatureSymbol: 'celsius',
  windowSize: {
    width: 0,
    height: 0
  }
};

const setTemperatureSymbol = (state, action) => Object.assign({}, state, { temperatureSymbol: action.payload });

const setSort = (state, action) =>
  Object.assign({}, state, {
    sortObject: action.payload.sort,
    cities: action.payload.cities
  });

const setWeatherDataFetchError = (state, action) => Object.assign({}, state, { hasErrored: action.payload });

const setWeatherDataFetchStatus = (state, action) => Object.assign({}, state, { isLoading: action.payload });

const setWeatherDataFetchSuccess = (state, action) => Object.assign({}, state, { cities: [...action.payload] });

const setWindowSize = (state, action) => Object.assign({}, state, { windowSize: action.payload });

const rootReducer = (state = DEFAULT_STATE, action) => {
  switch (action.type) {
    case GET_WEATHER_DATA:
      return setWeatherDataFetchError(state, action);
    case WEATHER_DATA_HAS_ERRORED:
      return setWeatherDataFetchError(state, action);
    case WEATHER_DATA_IS_LOADING:
      return setWeatherDataFetchStatus(state, action);
    case WEATHER_DATA_FETCH_SUCCESS:
      return setWeatherDataFetchSuccess(state, action);
    case SET_SORT:
      return setSort(state, action);
    case SET_TEMPERATURE_SYMBOL:
      return setTemperatureSymbol(state, action);
    case GET_WINDOW_SIZE:
      return setWindowSize(state, action);
    default:
      return state;
  }
};

export default rootReducer;
