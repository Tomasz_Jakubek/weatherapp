import axios from 'axios';
import {
  WEATHER_DATA_HAS_ERRORED,
  WEATHER_DATA_IS_LOADING,
  WEATHER_DATA_FETCH_SUCCESS,
  SET_SORT,
  SET_TEMPERATURE_SYMBOL,
  GET_WINDOW_SIZE
} from './actions';

export function setSort(data) {
  return {
    type: SET_SORT,
    payload: data
  };
}

export function setTemperatureSymbol(value) {
  return {
    type: SET_TEMPERATURE_SYMBOL,
    payload: value
  };
}

export function weatherDataHasErrored(bool) {
  return {
    type: WEATHER_DATA_HAS_ERRORED,
    payload: bool
  };
}

export function weatherDataIsLoading(bool) {
  return {
    type: WEATHER_DATA_IS_LOADING,
    payload: bool
  };
}

export function weatherDataFetchSuccess(weatherData) {
  return {
    type: WEATHER_DATA_FETCH_SUCCESS,
    payload: weatherData
  };
}
export function getWindowSize(windowSize) {
  return {
    type: GET_WINDOW_SIZE,
    payload: windowSize
  };
}

export function getWeatherData(url) {
  return dispatch => {
    dispatch(weatherDataIsLoading(true));
    axios
      .get(url)
      .then(response => {
        dispatch(weatherDataIsLoading(false));
        const sortedCities = response.data.list.map(el => ({
          cloudCover: el.clouds.all,
          temperature: el.main.temp,
          pressure: el.main.pressure.toFixed(0),
          humidity: el.main.humidity,
          weatherDesc: el.weather[0].description,
          icon: el.weather[0].icon,
          name: el.name,
          windSpeed: el.wind.speed,
          windDirection: el.wind.deg
        }));
        dispatch(weatherDataFetchSuccess(sortedCities));
      })
      .catch(() => dispatch(weatherDataHasErrored(true)));
  };
}
